import java.util.Scanner;

public class Kugel {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner in = new Scanner (System.in);
		
		System.out.println("Geben Sie bitte den wert f�r ihren Radius ein.");
		double r = in.nextDouble();
		
		
		double V = kugelVolumen(r);
		
		System.out.printf("4/3 * %.2f * %.2f * %.2f * PI = %.4f\n" ,r , r, r, V);

	}
	
	public static double kugelVolumen (double radius) {
		final double PI = Math.PI;
		return radius * radius * radius * 4/3 * PI ;
	}

}
