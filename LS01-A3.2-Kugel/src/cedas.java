import java.util.Scanner;

public class cedas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner in = new Scanner (System.in);
		
		System.out.println("Geben Sie bitte den wert f�r ihren Radius ein.");
		double r = in.nextDouble();
		
		
		double V = kugelVolumen(r);
		
		System.out.printf("%.2f * %.2f * %.2f = %.2\n" ,r , r, r, V);

	}
	
	public static double kugelVolumen (double radius) {
		final double PI = Math.PI;
		return (4/3) * radius * radius * radius * PI;
	}

}
