import java.util.Scanner;

public class SayYourName {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben Sie ihren Vornamen ein");
		String vornamen = myScanner.next();
		
		System.out.println("Geben Sie ihren Nachnamen ein");
		String nachname = myScanner.next();
		
		System.out.println("Guten Tag Herr " + nachname);
		
		System.out.println("Wie geht es Ihnen?");

	}

}
